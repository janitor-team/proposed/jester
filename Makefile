# makefile for jester

CC = gcc -Wall -O2
#CC = gcc -Wall -g
LIBS = -L/usr/X11R6/lib -lX11
INSTALLDIR = /usr/local

jester: jester.c jester.h
	$(CC) -o jester jester.c $(LIBDIR) $(LIBS)

install: jester jester.6
	install jester $(INSTALLDIR)/bin/jester
	install -m 644 jester.6 $(INSTALLDIR)/man/man6/jester.6

clean:
	rm -f jester
